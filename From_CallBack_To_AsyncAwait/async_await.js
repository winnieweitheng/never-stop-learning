//REFERENCE: https://medium.freecodecamp.org/javascript-from-callbacks-to-async-await-1cc090ddad99

/**START: Synchronous  */
console.log('1');
console.log('2');
console.log('3');
/**END: Synchronous*/

/**========================================================================================== */

/**START: Asynchronous */
console.log('1');
setTimeout(function afterTwoSeconds(){
    console.log('2');
}, 2000);
console.log('3');
/**ENDL Asynchronous */

/**========================================================================================== */

/**START: Example starter */
let url = 'https://api.github.com/users/daspinola/repos';
request(url);
function request(url) {
    const xhr = new XMLHttpRequest();
    xhr.timeout = 2000;
    xhr.onreadystatechange = function(e) {
        // 4 = request had been sent, server had finished returning the response, borwser had finished downloading the response content, AJAX call has completed
        if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                console.log('success');
            } else {
                console.log('not success');
            }
        }
    }
    xhr.ontimeout = function() {
        // if code timeout
        console.log('code time out');
    }
    xhr.open('get', url, true)//method: string, url: string, aync: boolean
    xhr.send();
}
/**END: Example starter */

/**========================================================================================== */

/**START: Callback */
function request(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.timeout = 2000;
    xhr.onreadystatechange = function(e) {
        if(xhr.readyState === 4) {
            if(xhr.status === 200) {
                callback(null, xhr.response);
            } else {
                callback(xhr.status, null);
            }
        }
    }
    xhr.ontimeout = function(){
        console.log('code time out');
    }
    xhr.open('get', url, true);
    xhr.send();
}
/**END: Callback */

/**START: request */
const userGetUrl = `https://api.github.com/search/users?page=1&q=daspinola&type=Users`;
//1. make a request to get a user's repositories
//2. after the request is complete, we use callback handleUsersList
request(userGetUrl, function handleUsersList(error, users){
    if(errpr) throw error;
    //3. If no error, then parse server response into an object using JSON.parse
    const list = JSON.parse(users).items

    //4. iterate user list. For each user, request their repositories list
    list.forEach(function(user){
        //5. When request has completed the callback, call handleReposList
        request(user.repos_url, function handleReposList(err, repos){
            if(err) throw err;
            // handler the repositories list here
        });
    });
})
/**END: request */

/**START: request 2 */
try {
    request(userGetUrl, handleUsersList)
} catch (e) {
    console.error('request error', e);
}

function handleUsersList(error, users) {
    if(error) throw error;
    const list = JSON.parse(users).items
    
    list.forEach(function(user){
        request(user.repos_url, handleReposList);
    }); 
}

function handleReposList(err, repos) {
    if(err) throw err;
    // Handle the repositories list here
    console.log('repositories', repos);
}
/**END: request 2 */

/**========================================================================================== */

/**START: Promises */
//1. A promise is initialized with a function that has resolve and reject statement
const myPromise = new Promise(function(resolve, reject){
    //2. Make your async code inside the Promise function
    //code here
    if(codeIsFine) {
        //2a. resolve when everything happens as desired
        resolve('fine');
    } else {
        //2b. reject when error
        reject('error');
    }
});

myPromise
    //3a. When a resolve is found, .then method will execute for that Promise
    .then(function whenOK(response){
        console.log(response);
        return response;
    })
    //3b. When a reject is found, .catch will be triggered
    .catch(function notOK(err){
        console.error(err);
    });
//4. resolve and reject only accept ONE parameter
/**END: Promises */

/**START: Promises Example */
function request(url) {
    return new Promise(function (resolve, reject){
        const xhr = new XMLHttpRequest();
        xhr.timeout = 2000;
        xhr.onreadystatechange = function(e){
            if(xhr.readyState === 4) {
                if(xhr.status === 200) {
                    resolve(xhr.response);
                } else {
                    reject(xhr.status);
                }
            }
        }
        xhr.ontimeout = function() {
            reject('timeout');
        }
        xhr.open('get', url, true);
        xhr.send();
    })
}
/**END: Promises Example */

/**START: Promises request */
const userGet = `https://api.github.com/search/users?page=1&q=daspinola&type=Users`;

const myPromise = request(userGet);
console.log('will be pending when logged', myPromise);

myPromise
    .then(function handleUsersList(users){
        console.log('when resolved is found it comes here with the response, in this case users ', users)
        const list = JSON.parse(users).items;
        return Promise.all(list.map(function(user){
            return request(user.repos_url)
        }))
    })
    .then(function handleReposList(repos){
        console.log('All users repos in an array', repos)
    })
    .catch(function handleErrors(error){
        console.log('When a reject is executed it will come here ignoring the then statement', error)
    })
/**END: Promises request */

/**START: Promises request code improvement */
const userGet = `https://api.github.com/search/users?page=1&q=daspinola&type=Users`;
const userRequest = request(userGet);
// Just by reading this part out loud you have a good idea of what the code does
userRequest
    .then(handleUsersList)
    .then(repoRequest)
    .then(handleReposList)
    .catch(handleErrors)

function handleUsersList(users){
    return JSON.parse(users).items;
}

function repoRequest(users){
    return Promise.all(users.map(function(user){
        return request(user.repos_url)
    }))
}

function handleReposList(repos) {
    console.log('All users repos in an array', repos)
}

function handleErrors(error) {
    console.error('something went wrong', error)
}
/**END: Promises request code improvement */

/**========================================================================================== */

/**START: Generators */
function* foo() {
    yield 1
    const args = yield 2
    console.log(args)
}

var fooIterator = foo();
console.log(fooIterator.next().value) // will log 1
console.log(fooIterator.next().value) // will log 2

fooIterator.next('aParam')// will log the console.log inside the generator 'aParam'
/**END: Generators */

/**START: Generator request */
function request(url) {
    return function(callback){
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(e) {
            if(xhr.readyState === 4) {
                if(xhr.status === 200) {
                    //2. Return a function reference expecting a callback for the first request
                    //request function accept a url, then return a function that expects a callback
                    callback(null, xhr.response);
                } else {
                    callback(xhr.status. null)
                }
            }
        }
        xhr.ontimeout = function() {
            console.log('timeout')
        }
        xhr.open('get', url, true);
        xhr.send();
    }
}
/**END: Generator request */

/**START: Generator example */
function* list() {
    const userGet = `https://api.github.com/search/users?page=1&q=daspinola&type=Users`;
    //1. wait until the first request is prepared
    const users = yield request(userGet)
    //3. expect a users to be sent in the next .next, it will go to 3a
    yield
    // 4. Iterate over users
    for(let i = 0; i< users.length; i++){
        //5. wait for the .nect for each of the users, it will go to 5a
        yield request(users[i].repos_url)
    }
}

try{
    
    const iterator = list();
    //3a
    iterator.next().value(function handleUsersList(err, users){
        if(err) throw err
        const list = JSON.parse(users).items
        //send the list of users for the iterator
        //5a return their respective callback function
        iterator.next(list);

        list.forEach(function(user){
            iterator.next().value(function userRepos(error, repos){
                if(error) throw repos
                // Handle each individual user repo here
                console.log(user, JSON.parse(repos))
            })
        })
    })
} catch (e) {
    console.log(e)
}
/**END: Generator example */

/**========================================================================================== */
/**START: Async/Awaits */
sumTwentyAfterTwoSeconds(10)
    .then(result => console.log('after 2 seconds', result));

//1. async function
async function sumTwentyAfterTwoSeconds(value) {
    //2. we tell our code to wait for the resolve or reject for our promise function afterTwoSeconds
    const remainder = afterTwoSeconds(20);
    //3. It will only end up in the .then when the await operations finish 
    return value + await remainder;
}

function afterTwoSeconds(value) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(value)
        }, 2000);
    });
}
/**END: Async/Awaits */

/**START: Async/Awaits example */
function request(url){
    return new Promise(function(resolve, reject){
        const xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(e) {
            if(xhr.readyState === 4) {
                if(xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.status)
                }
            }
        }
        xhr.ontimeout = function() {
            reject('timeout');
        }
        xhr.open('get', url, true);
        xhr.send();
    })
}

//1. Async list function that will handle the requests
async function list() {
    const userGet = `https://api.github.com/search/users?page=1&q=daspinola&type=Users`;
    const users = await request(userGet);
    const userList = JSON.parse(users).items;

    //2. Another async is needed in the forEach so that we have the list of repos for each user to manipulate
    userList.forEach(async function(user){
        const repos = await request(user.repos_url);
        handleReposList(user, repos);
    });
}

function handleReposList(user, repos){
    const userRepos = JSON.parse(repos)
    //Handle each individual user repo here
    console.log(user, userRepos)
}
/**END: Async/Awaits example */